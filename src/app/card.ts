export interface Card {
  number: number;
  multiverseid: number;
  name: string;
  manaCost: string;
  type: string;
  rarity: string;
  text: string;
  imageUrl: string;
}
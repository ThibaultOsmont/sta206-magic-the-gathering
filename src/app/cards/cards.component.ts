import { Component, OnInit } from '@angular/core';
import { CardService } from '../card.service';
import { Card } from '../card';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  cardList: Card[];
  cards: Card[];
  rarities: Set<string>;

  isLoading: boolean;

  constructor(private cardService: CardService) {
    this.rarities = new Set<string>();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.cardService.getCards().subscribe(cardList => {
      this.cardList = cardList.cards;
      this.cards = this.cardList.filter(card => card.multiverseid !== undefined);

      this.cards.forEach(card => this.rarities.add(card.rarity));
      this.isLoading = false;
    });
  }

  onRarityChange(rarity: string) {
    this.cards = this.cardList
      .filter(card => card.multiverseid !== undefined)
      .filter(card => card.rarity === rarity);
  }
}

import { Component, OnInit } from '@angular/core';
import { Card } from '../card';
import { CardService } from '../card.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss']
})
export class CardDetailComponent implements OnInit {

  cardId: number;
  card: Card;
  isLoading: boolean;

  constructor(private route: ActivatedRoute,
    private cardService: CardService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe(params => {
      this.cardId = +params['id']; // Reminder: + converts a string to a number

      this.cardService.getById(this.cardId).subscribe(cardInfo => {
        this.card = cardInfo.card;
        this.isLoading = false;
      });
    });
  }

}

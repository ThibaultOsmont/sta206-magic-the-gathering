import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Card } from './card';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private cardsUrl = 'https://api.magicthegathering.io/v1/cards';

  constructor(private http: HttpClient) { }

  public getCards(): Observable<any> {
    return this.http.get<Card[]>(this.cardsUrl, {
      params: new HttpParams().append('random', 'true')
    });
  }

  public getById(id: number): Observable<any> {
    return this.http.get<Card>(this.cardsUrl + `/${id}`);
  }
}

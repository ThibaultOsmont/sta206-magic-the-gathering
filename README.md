# STA206 Magic The Gathering

Projet de (re)familiarisation avec Angular. Utilisation de l'API https://api.magicthegathering.io/v1/cards

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.


## Installation

```bash
npm install
```

## Démarrage

```bash
npm start
```

## Objectifs (pour se familiariser avec [Ember.js](https://emberjs.com/)):
- [x] Call l' API
- [x] Afficher une liste de cartes
- [] Recherche par nom de carte
- [x] Filtrer par rareté
- [x] Cliquer sur une carte qui amène a une page qui affiche plus de détails avec ember-bootsrap + du beau CSS

## Dépendances supplémentaires utilisées

[Angular CLI](https://cli.angular.io)

[bootstrap 4.5.2](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

## Documentation Angular
[API de Magic The Gathering](https://api.magicthegathering.io/v1/cards)

[Documentation de l'API](https://docs.magicthegathering.io/)

[Angular tutoriel](https://angular.io/tutorial)

## Documentation auto-générée par [Angular CLI](https://cli.angular.io)

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
